
@DevPortal
Feature: Title of your feature
	I want to use this template for my feature file


Background:
Given Developer Portal is opened	 

#************************************Templates*********************************************************
@APP_Templates_GetAddons
Scenario Outline: API Templates_GetAddons
And I select Application
And I select Templates_GetAddons
And I set query parameter "partProvider" to <partProvider>
And I set query parameter "partId" to <partID>
When I click on Send this request button
Then response code should be 200

Examples:
	|partProvider          | partID             |
	|"Merchant"            | "Gateway"          |
	|"SagePaymentSolutions"| "Gateway"          |
	|"ISO"                 | "Gateway"          |
	|"Merchant"            | "Software"         |
	|"SagePaymentSolutions"| "Software"         |
	|"ISO"                 | "Software"         |
	|"Merchant"            | "Terminal"         |
	|"SagePaymentSolutions"| "Terminal"         |
	|"ISO"                 | "Terminal"         |
	|"Merchant"            | "Terminal/Printer" |
	|"SagePaymentSolutions"| "Gateway/Printer"  |
  |"ISO"                 | "Gateway/Printer"  |  


@APP_Templates_GetAdvanceFundingProcessors
Scenario: API Templates_GetAdvanceFundingProcessors
And I select Application
And I select Templates_GetAdvanceFundingProcessors
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

@APP_Templates_GetAssociations
Scenario: API Templates_GetAssociations
And I select Application
And I select Templates_GetAssociations
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

@APP_Templates_GetDiscountPaidFrequencies
Scenario: API Templates_GetDiscountPaidFrequencies
And I select Application
And I select Templates_GetDiscountPaidFrequencies
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

@APP_Templates_GetEquipment
Scenario Outline: API Templates_GetEquipment
And I select Application
And I select Templates_GetEquipment
And I set query parameter "partProvider" to <partProvider>
And I set query parameter "partType" to <partType>
When I click on Send this request button
Then response code should be 200
#And response body should contain "id"
#And response body should contain "title"
#And response body should contain "isDefault"
#And response body should contain "canEdit"

Examples:
	|partProvider          | partType             |
	|"Merchant"            | "Gateway"          |
	|"SagePaymentSolutions"| "Gateway"          |
	|"ISO"                 | "Gateway"          |
	|"Merchant"            | "Software"         |
	|"SagePaymentSolutions"| "Software"         |
	|"ISO"                 | "Software"         |
	|"Merchant"            | "Terminal"         |
	|"SagePaymentSolutions"| "Terminal"         |
	|"ISO"                 | "Terminal"         |
	|"Merchant"            | "TerminalOrPrinter" |
	|"SagePaymentSolutions"| "TerminalOrPrinter"  |
  |"ISO"                 | "TerminalOrPrinter"  |  

@APP_Templates_GetEquipmentPrograms
Scenario Outline: API Templates_GetEquipmentPrograms
And I select Application
And I select Templates_GetEquipmentPrograms
And I set query parameter "partId" to <partID>
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

Examples:
| partID             |
| "Gateway"          |
| "Software"         |
| "Terminal"         |
| "Terminal/Printer" |


@APP_Templates_GetFanfTypes
Scenario: API Templates_GetFanfTypes
And I select Application
And I select Templates_GetFanfTypes
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

@APP_Templates_GetFees
Scenario: API Templates_GetFees
And I select Application
And I select Templates_GetFees
When I click on Send this request button
Then response code should be 200
And response body should contain "product"
And response body should contain "fees"


@APP_Templates_GetLeadSources
Scenario: API Templates_GetLeadSources
And I select Application
And I select Templates_GetLeadSources
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

@APP_Templates_GetPinDebitInterchangeTypes
Scenario: API Templates_GetPinDebitInterchangeTypes
And I select Application
And I select Templates_GetPinDebitInterchangeTypes
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

@APP_Templates_GetProducts
Scenario: API Templates_GetProducts
And I select Application
And I select Templates_GetProducts
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

@APP_Templates_GetReferralGroups
Scenario: API Templates_GetReferralGroups
And I select Application
And I select Templates_GetReferralGroups
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

@APP_Templates_GetSettlementTypes
Scenario: API Templates_GetSettlementTypes
And I select Application
And I select Templates_GetSettlementTypes
When I click on Send this request button
Then response code should be 200
And response body should contain "id"
And response body should contain "title"
And response body should contain "isDefault"
And response body should contain "canEdit"

