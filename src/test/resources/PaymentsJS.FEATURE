Feature: Verify PaymentsJS functionality

  @VerifyPaymentJSSubmit
  Scenario Outline: Performing a payment Transaction using PaymentsJS
    Given In developer Portal click on PaymentsJS > Try It option.
    And I set cardNumber to <CardNumber>.
    And I set cardExpiration Month  to <ExpirationMonth> and year to <ExpirationYearIndex>.
    And I set cvv to <CVV>.  
    When I click on Submit.
    Then response message should be 'Approved'.

    Examples: 
      | CardName  | CardNumber       | ExpirationMonth | ExpirationYearIndex | CVV |
      | Visa      | 4111111111111111 |            4 |                   2 | 123 |
      |	Mastercard|	5454545454545454 |			 				6  |                   5 | 123 |					
		  |	Discover	|	6011000993026909 |               7 |                   6 | 123 |
	    |	Amex		  |	371449635392376  |               12 |                  7 | 1234|
		  
	@VerifyPaymentJSLink
  Scenario: Verify PaymentsJS link functionality
    Given In developer Portal click on PaymentsJS.
    Then It should display Payments.JS information with 'TRY IT!' option enabled
		And  It should show the path Home Documentation Sage Payment solutions Payments.js in the middle right corner of the page
		When Clicked on the Sage Payment Solutions link which is present in the middle right corner of the page
		Then It should go back one step i.e Sage Payment solutions should be selected
		When Clicked on the Documentation link which is present in the middle right corner of the page
		Then It should go back one step i.e Documentation should be selected
		When Clicked on 'Documentation : View  on GIT' icon
		Then It should open Git hub link showing header Sage Payment Solutions
		
	@VerifyPaymentJSUI
  Scenario Outline: Verify PaymentsJS UI functionality
    Given In developer Portal click on PaymentsJS > Try It option.
    Then Default text shown in txtCardNum field is Enter the credit card number
    And Default text shown in txtCVV field is Enter 3 or 4 digit security code
    When I set cardNumber to <CardNumber>.
    Then <CardName> cardicon should be enabled
    When I set cardNumber to 4321234.
    Then Invalid number warning message should be displayed
    When I set cardNumber to 545454545454545454545454.
    Then It should allow to enter only 16 digits i.e '5454545454545454'
    When I set cardNumber to abc123.
    Then It should not accept alphabets i.e 123
    When I set cardExpiration Month  to 1 and year to 1.
    Then 'Credit card expired' warning message should be displayed
    When Click on Expiration Year combo box
    Then It should list current year and additional 14 years
     Examples: 
     | CardName  		  | CardNumber       |
     | VisaIcon       | 4111111111111111 |
     |	MastercardIcon|	5454545454545454 |					
		 |	DiscoverIcon	|	6011000993026909 |
	   |	AmexIcon		  |	371449635392376  |