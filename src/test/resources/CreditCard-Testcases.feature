#Feature: Payment Center credit card transaction test
    #I Want to test Payment Center application credit card transaction related functionality    
#
#@CreditCardTransaction
#Scenario Outline: Perform different types of credit card transaction
#Given  Login to Payment center application
#And Launch Accept Payment screen
#When Perform a creditcard <TxType> Transaction using card <CardNum>
#Then Approved response should be displayed
#Examples:
#|TxType|CardNum|
#|Sale|4111111111111111|
#|Auth Only|5499740000000057|
#|Force|6011000993026909|
#|Credit|371449635392376|


#@CCTxReqFieldvalidation
#Scenario: Verify when Required fields are left blank with no values entered
#Given  Login to Payment center application
#And Launch Accept Payment screen
#When Enter null data in required fields
#|txtSubTotal|
#Then Warning message should be displayed
#|Enter an amount greater than 0.01|warningMsgAmountNoVal|
#When Enter null data in required fields
#|txtShipping|
#Then Warning message should be displayed
#|Enter an amount greater than 0.01|warningMsgAmountNoVal|
#When Enter null data in required fields
#|txtTax|
#Then Warning message should be displayed
#|Enter an amount greater than 0.01|warningMsgAmountNoVal|												 
#When Enter null data in required fields
#|txtCardNumber|
#Then Warning message should be displayed
#|Required field|warningMsgReqField|
#When Enter null data in required fields
#|txtBillingFirstName|
#Then Warning message should be displayed
#|Required field|warningMsgReqField|
#When Enter null data in required fields
#|txtBillingLastName|
#Then Warning message should be displayed
#|Required field|warningMsgReqField|
#When Enter null data in required fields
#|txtBillingZip|
#Then Warning message should be displayed
#|Required field|warningMsgReqField|

#@CreditCardDeclinedTransaction
#Scenario:perform credit card declined transaction
#Given  Login to Payment center application
#When Perform a creditcard Transaction for amount 2, Tax 0 and Shipping 0
#Then Declined response should be displayed
#
#@VerifyCCTaxShippingData
#Scenario Outline: Perform a credit card transaction and verify the tax ,shipping amount passed are retained
#Given  Login to Payment center application
#When Perform a creditcard Transaction for amount <Amount>, Tax <Tax> and Shipping <Shipping>
#Then Approved response should be displayed
#When I select open Transactions > Credit Card option
#And select the above performed Credit Card transaction reference
#Then Tax <Tax> & Shipping <Shipping> info passed by user shows up retained
#Examples:
#|Amount|Tax|Shipping|
#|1|1|1|
#|3|0|3|
#|5|1|0|
#
#
#@CreditCardInvalidCharValidation
#Scenario: perform invalid characters validation in creditcard fields
#Given  Login to Payment center application
#And Launch Accept Payment screen
#When Enter wrong data in required fields
#|txtSubTotal|(@^)|
#Then Warning message should be displayed
#|Must include one or two decimal places and cannot be negative|warningMsgAmount|
#When Enter wrong data in required fields
#|txtShipping|(@^)|
#Then Warning message should be displayed
#|Must include one or two decimal places and cannot be negative|warningMsgAmount|
#When Enter wrong data in required fields
#|txtTax|(@^)|
#Then Warning message should be displayed
#|Must include one or two decimal places and cannot be negative|warningMsgAmount|
#When Enter wrong data in required fields
#|txtCardNumber|(1234)|
#Then Warning message should be displayed
#|Enter the sixteen digit number|warningMsgCCNum|
#When Enter wrong data in required fields
#|txtBillingFirstName|(@!(|
#Then Warning message should be displayed
#|Please remove invalid characters, such as *, & are not allowed|warningMsgFirstName|
#When Enter wrong data in required fields
#|txtBillingLastName||
#Then Warning message should be displayed
#|Required field|warningMsgLastName|
#When Enter wrong data in required fields
#|txtBillingZip|(@!(|
#Then Warning message should be displayed
#|Enter a valid ZIP or postal code|warningMsgZip| 




#@CreditCardHideExtraFields
#Scenario: Perform a transaction hiding extra fields
#Given  Login to Payment center application
#And Launch Accept Payment screen
#When HideExtraFields is checked
#Then It should hide Address,City,Zip,State,country fields
#When Perform a transaction by hiding extra fields
#Then Approved response should be displayed
#
#@CreditCardCancelTx
#Scenario: Verify Cancel operation
#Given  Login to Payment center application
#And Launch Accept Payment screen
#And Enter data in Accept Payments > Credit Card screen
#When Cancel is clicked and No option is selected
#Then Data in Credit Card screen should remain
#When Cancel is clicked and Yes option is selected
#Then Data should reset in all the fields


#@CreditCardAddRemoveShipAddress
#Scenario: Verify adding/removing shipping address
#Given  Login to Payment center application
#And Launch Accept Payment screen
#When Add Shipping Address is clicked
#Then Shipping Information section appears
#When Remove Shipping Address is clicked
#Then Shipping Information section disappears
#
#
#@CreditCardTxSameBillShipAddress
#Scenario: Verify cc transaction by adding same biiling address as shipping address
#Given  Login to Payment center application
#And Launch Accept Payment screen
#And Enter data in Accept Payments > Credit Card screen
#When Add Shipping Address is clicked
#Then Shipping Information section appears
#When Select checkbox Use Billing Address
#Then Billing address data should be copied to Shiping address fields
#When Perform the transaction with above data
#Then Approved response should be displayed
#
#@CreditCardTxDiffBillShipAddress
#Scenario: Verify cc transaction by adding different biiling address from shipping address
#Given  Login to Payment center application
#And Launch Accept Payment screen
#And Enter data in Accept Payments > Credit Card screen
#When Add Shipping Address is clicked
#Then Shipping Information section appears
#When Enter different shipping data in Accept Payments screen
#And Perform the transaction with above data
#Then Approved response should be displayed

#
#@CreditCardClearBtn
#Scenario: Verify Clear button functionality
#Given  Login to Payment center application
#And Launch Accept Payment screen
#And Enter data in Credit Card Payment method section
#When Clear button is clicked
#Then It should reset the values in CardNumber,ExpMonth,ExpYear,CVV fields


#@CreditCardTxSessionTimeout
#Scenario: Verify session timeout happens after 15 min
#Given  Login to Payment center application
#And Launch Accept Payment screen
#When Application is idle for 10 minutes
#Then It should popup Inactive session dialogue


#@CreditCardPurchaseCardTx
#Scenario Outline: Perform different types of credit card transaction using purchase card
#Given  Login to Payment center application
#And Launch Accept Payment screen
#When Perform a creditcard <TxType> Transaction using card <CardNum>
#Then Approved response should be displayed
#Examples:
#|TxType|CardNum|
#|Sale|4128123412341231|
#|Auth Only|5499740000000065|
#|Force|5499740000000065|
#|Credit|4128123412341231|


