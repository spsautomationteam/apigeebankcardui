Feature: Verify Tokenization functionality

    @VerifyProcessToken
  Scenario Outline: Create a token and then performing a payment Transaction using Token.
    Given In developer Portal click on PaymentsJS >Tokenization > Tokenize card option.
    And I set cardNumber to <CardNumber>.
    And I set cardExpiration Month  to <ExpirationMonth> and year to <ExpirationYearIndex>.
    When I click on Submit.
    And I click on Process Token.
    Then response should be 'Approved'.
    
 Examples: 
      | CardName  | CardNumber       | ExpirationMonth | ExpirationYearIndex |
      | Visa      | 4111111111111111 |               1 |                   2 | 
      |	Mastercard|	5454545454545454 |			 				3   |                   5 | 			
#	 	  |	Discover  |	6011000993026909 |               7 |                   6 | 
#	  	|	Amex	  |	371449635392376  |               12  |                  7  |