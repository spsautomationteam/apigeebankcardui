package Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelFunctions {
	
		//public static final String Path_TestData = "C:/Temp/Eclipse/WorkSpace/AutomationTesting/InputData/";
		
	    public static final String Path_TestData = System.getProperty("user.dir")+"\\InputData\\InputData.xls";

		//public static final String File_TestData = "InputData.xls";

  		private static HSSFSheet ExcelWSheet;

  		private static HSSFWorkbook ExcelWBook;

  		private static HSSFCell Cell;

  		private static HSSFRow Row;

		//This method is to set the File path and to open the Excel file, Pass Excel Path and Sheetname as Arguments to this method

		public static void setExcelFile(String Path,String SheetName) throws Exception {

 			try {

     			// Open the Excel file

				FileInputStream ExcelFile = new FileInputStream(Path);

				// Access the required test data sheet

				ExcelWBook = new HSSFWorkbook(ExcelFile);

				ExcelWSheet = ExcelWBook.getSheet(SheetName);

				} catch (Exception e){

					throw (e);

				}

		}

		//This method is to read the test data from the Excel cell, in this we are passing parameters as Row num and Col num

	    public static String getCellData(String RowLabel, int ColNum) throws Exception{

 			try{

 				//Create a loop over all the rows of excel file to read it
 			    int rowCount = ExcelWSheet.getPhysicalNumberOfRows();
 			    int colCount = ExcelWSheet.getRow(0).getPhysicalNumberOfCells();
 			    String cellVal=null;
 			    Found:
 				   {
 			    	for (int col = 0; col < colCount; col++) 
 				   
 			    	{
 			    	for (int row = 0; row < rowCount; row++) 
 			    		{
 			    		if(ExcelWSheet.getRow(row).getCell(col).getStringCellValue().equals(RowLabel))
 			    		     {    		    	  
 			    				cellVal =ExcelWSheet.getRow(row).getCell(ColNum).getStringCellValue();
 			    		    	break Found;
 			    		     }
 			    		 }
 			    	}
 				   }
 			    return cellVal;
 	
    			}catch (Exception e){

					return"";

    			}

	    }   
	    
}
 
 