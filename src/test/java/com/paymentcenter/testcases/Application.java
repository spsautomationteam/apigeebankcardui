package com.paymentcenter.testcases;

import org.openqa.selenium.WebDriver;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utility.ExcelFunctions;
import Utility.ProjSpecificFunctions;
import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Application {
	
	public  WebDriver driver;
	private String data;
	public static String referenceNum;
	public static String OrderNum;
	public static String cmbMonth;
	public static String cmbYear;
	
	
	public Application()
	{
		driver=Hooks.driver;
	}
	
	@Given("^I select Application$")
	public void i_select_Application() throws Throwable {
		
		UtilityFunctions.performAction("panelApplication","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetAddons$")
	public void i_select_Templates_GetAddons() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetAddons","");	
		Thread.sleep(2000);

	}	
	
	@Given("^I select Templates_GetAdvanceFundingProcessors$")
	public void i_select_Templates_GetAdvanceFundingProcessors() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetAdvanceFundingProcessors","");	
		Thread.sleep(2000);

	}	
	
	
	@Given("^I select Templates_GetDiscountPaidFrequencies$")
	public void i_select_Templates_GetDiscountPaidFrequencies() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetDiscountPaidFrequencies","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetEquipment$")
	public void i_select_Templates_GetEquipment() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetEquipment","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetEquipmentPrograms$")
	public void i_select_Templates_GetEquipmentPrograms() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetEquipmentPrograms","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetFanfTypes$")
	public void i_select_Templates_GetFanfTypes() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetFanfTypes","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetFees$")
	public void i_select_Templates_GetFees() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetFees","");	
		Thread.sleep(2000);

	}
	
	
	@Given("^I select Templates_GetLeadSources$")
	public void i_select_Templates_GetLeadSources() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetLeadSources","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetPinDebitInterchangeTypes$")
	public void i_select_Templates_GetPinDebitInterchangeTypes() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetPinDebitInterchangeTypes","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetAssociations$")
	public void i_select_Templates_GetAssociations() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetAssociations","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetProducts$")
	public void i_select_Templates_GetProducts() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetProducts","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetReferralGroups$")
	public void i_select_Templates_GetReferralGroups() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetReferralGroups","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Templates_GetSettlementTypes$")
	public void i_select_TemplatesGet_SettlementTypes() throws Throwable {
		
		UtilityFunctions.performAction("panelTemplatesGetSettlementTypes","");	
		Thread.sleep(2000);

	}

}
