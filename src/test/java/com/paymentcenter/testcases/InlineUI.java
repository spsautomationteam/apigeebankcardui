package com.paymentcenter.testcases;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


//import org.testng.annotations.Test;


import com.sun.jna.platform.FileUtils;

import Utility.UtilityFunctions;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class InlineUI
{
	public  WebDriver driver;
	public static String strjSON;
	//Scenario scenario;
	public InlineUI()
	{	
	driver = Hooks.driver; 	
	}
	
	
	@Test
	@Given("^In developer Portal click on PaymentsJS >Inline UI option.$")
	public void Click_InlineUI_TryIt() throws Throwable 
	{
		UtilityFunctions.performAction("imgPayJS","");
		UtilityFunctions.performAction("linkInlineUI","");
	}
	
	
	@Then("^Inline transaction response message should be 'Approved'.$")
	public void verify_response_message() throws Throwable 
	{
		Thread.sleep(25000);
		UtilityFunctions.performValidation("responseSuccessMsg","Approved!");		
		UtilityFunctions.performValidation("jsonPymntResponse","Approved");
		
	}
	
	@After
	public void display_Response(Scenario scenario)
	{
		scenario.write(strjSON);
		System.out.println("test end first");
		
	
		scenario.write(strjSON);
	}

}
