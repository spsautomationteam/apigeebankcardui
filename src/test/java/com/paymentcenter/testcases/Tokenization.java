package com.paymentcenter.testcases;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import Utility.UtilityFunctions;

//import org.testng.annotations.Test;


//import com.sun.jna.platform.FileUtils;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Tokenization
{
	public  WebDriver driver;
	
	public Tokenization()
	{	
	driver = Hooks.driver; 	
	}
	
	
	@Test
	@Given("^In developer Portal click on PaymentsJS >Tokenization > Tokenize card option.$")
	public void Click_Tokenization_Tokenize() throws Throwable 
	{
		UtilityFunctions.performAction("imgPayJS","");
		UtilityFunctions.performAction("linkTokenization","");
		UtilityFunctions.performAction("btnVault","");

	}
	
	@When("^I click on Process Token.$")
	public void click_ProcessToken() throws Throwable 
	{
		//close the result popup
		UtilityFunctions.performAction("btnClose","");
		
		//Click on Process Token
		UtilityFunctions.performAction("btnProcessToken","");
		
	}

	@Then("^response should be 'Approved'.$")
	public void verify_response_message() throws Throwable 
	{
		String str="";
		Thread.sleep(25000);
		WebElement successMessage =  UtilityFunctions.retunWebElement("tokenresponseDialog");
		if(successMessage.isDisplayed())
		{
			Thread.sleep(5000);
			UtilityFunctions.performValidation("jsonPymntResponse","Approved");		
				
		}
   
	}
	
		

}
