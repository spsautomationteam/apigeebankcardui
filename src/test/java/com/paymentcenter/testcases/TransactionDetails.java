package com.paymentcenter.testcases;

import java.awt.AWTException;
import java.io.IOException;

import org.junit.Assert;

import Utility.UtilityFunctions;
import cucumber.api.java.en.Then;

public class TransactionDetails 
{
	@Then("^Tax (.*) & Shipping (.*) info passed by user shows up retained$")
	public void verifyTaxShippingAmount(String Tax,String Shipping) throws IOException, InterruptedException, AWTException
	{
		Assert.assertEquals("Transactions Details",Hooks.driver.getTitle());		
		UtilityFunctions.performAction("linkOrder", "");
		
		Assert.assertEquals("$"+Tax+".00",UtilityFunctions.retunWebElement("labelTaxAmount").getText());
		Assert.assertEquals(Shipping,UtilityFunctions.retunWebElement("labelShippingAmount").getText());
		
		
	}
	
	@Then("^It should display repective transaction details$")
	public void verifyTxDetails() throws IOException, InterruptedException, AWTException
	{
		Assert.assertEquals("Transactions Details",Hooks.driver.getTitle());	
		Assert.assertEquals(VirtualTerminalTransaction.referenceNum,UtilityFunctions.retunWebElement("labelTxDetailRefNum").getText());
		UtilityFunctions.performAction("linkOrder", "");
		Assert.assertEquals(VirtualTerminalTransaction.OrderNum,UtilityFunctions.retunWebElement("labelTxDetailOrderNum").getText());

	}
}
