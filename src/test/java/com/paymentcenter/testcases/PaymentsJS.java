package com.paymentcenter.testcases;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


import Utility.ExcelFunctions;
import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PaymentsJS
{
	public  WebDriver driver;
	
	public PaymentsJS()
	{	
	driver = Hooks.driver; 	
	}
	


	@Test
	@Given("^In developer Portal click on PaymentsJS > Try It option.$")
	public void Click_PaymentsJS_TryIt() throws Throwable 
	{
		UtilityFunctions.performAction("imgPayJS","");
		//Thread.sleep(5000);
		UtilityFunctions.performAction("btnPayJSTryIt","");
		//Thread.sleep(5000);
		UtilityFunctions.performValidation("payjsPopupTitle","Payment Details");
	}


		
	@Given("^I set cardNumber to (.*).$")
	public void set_cardNumber(String cardNum) throws Throwable 
	{	
		//Enter card number'
		UtilityFunctions.performAction("txtCardNum",cardNum);
		Thread.sleep(3000);
		
	}

	@Given("^I set cardExpiration Month  to (.*) and year to (.*)\\.$")
	public void set_cardExpiration_Month_year(String month, int year) throws Throwable 
	{
		UtilityFunctions.performAction("btnExpMonth","");
		
	    WebElement menuMonthName= driver.findElement(By.xpath(".//*[@id='sps-pay-form']/div[2]/div[1]/div/ul/li["+month+"]/a"));
		menuMonthName.click();
		
		UtilityFunctions.performAction("btnExpYear","");
					
		WebElement menuYearName= driver.findElement(By.xpath(".//*[@id='sps-pay-form']/div[2]/div[2]/div/ul/li["+year+"]/a"));
		menuYearName.click();	
		Thread.sleep(3000);
		
	}

	@Given("^I set cvv to (\\d+).$")
	public void set_cvv(String cvv) throws Throwable 
	{
		//Enter CVV		
		UtilityFunctions.performAction("txtCVV",cvv);
		
	}
	
	
	@Given("^In developer Portal click on PaymentsJS.$")
	public void click_PayJs() throws Throwable
	{
		UtilityFunctions.performAction("imgPayJS","");
	}
		
		

	@When("^I click on Submit.$")
	public void click_Submit() throws Throwable 
	{
		//Click on Submit
		UtilityFunctions.performAction("btnSubmit","");				
		Thread.sleep(5000);
		
		
	}

	@When("^Clicked on the (.*) link which is present in the middle right corner of the page$")
	public void click_pageLink(String pageLink) throws Throwable 
	{
		if (pageLink.equals("Sage Payment Solutions"))
		{
			UtilityFunctions.performAction("linkSagePymntSol","");				
		}
		else if(pageLink.equals("Documentation"))
		{
			UtilityFunctions.performAction("linkDocument","");
		}
			
		
	}
	

	@When("^Clicked on 'Documentation : View  on GIT' icon$")
	public void click_Gitlink() throws Throwable 
	{	
		    UtilityFunctions.performAction("linkPaymentsJS","");	
			UtilityFunctions.performAction("linkViewOnGit","");				

	}
	
	@When("^Click on Expiration Year combo box$")
	public void click_ExpYear() throws Throwable 
	{	
		    UtilityFunctions.performAction("btnExpYear","");	
				
	}
	
	
	
	
	
	
	
	

	@Then("^response message should be 'Approved'.$")
	public void verify_response_message() throws Throwable 
	{
		Thread.sleep(5000);
		WebElement successPopup = UtilityFunctions.retunWebElement("responsePopup") ;
		if(successPopup.isDisplayed())
		{
			UtilityFunctions.performValidation("responseSuccessMsg","Approved!");
			
		}
   
	}
	
	@Then("^It should display Payments.JS information with 'TRY IT!' option enabled$")
	public void verify_payJsField() throws Throwable 
	{
		UtilityFunctions.performValidation("headerPayJSTitle","Payments.js");
		UtilityFunctions.performValidation("btnPayJSTryIt","true");
	}

	
	@Then("^It should show the path (.*) in the middle right corner of the page$")
	public void verify_pagePath(String path) throws Throwable 	
	{
		UtilityFunctions.performValidation("ulPayjsPath",path);
	}
	
	@Then("^It should go back one step i.e (.*) should be selected$")
	public void verify_linkCLick(String path) throws Throwable 	
	{
		if (path.equals("Sage Payment Solutions"))
		{
			UtilityFunctions.performValidation("headerSagePymntSolTitle",path);				
		}
		else if(path.equals("Documentation"))
		{
			UtilityFunctions.performValidation("headerDocumentaion",path);
		}
	}
	
	@Then("^It should open Git hub link showing header (.*)$")
	public void verify_gitPageh(String header) throws Throwable 	
	{
		UtilityFunctions.performValidation("headerGit",header);
	}
	
	@Then("^Default text shown in (.*) field is (.*)$")
	public void verify_defaultText(String field,String data) throws Throwable 	
	{
		WebElement we=UtilityFunctions.retunWebElement(field);
		Assert.assertEquals(data,we.getAttribute("placeholder"));
	}
	
	@Then("^(.*) cardicon should be enabled$")
	public void verify_cardIcon(String card) throws Throwable 	
	{
		UtilityFunctions.performValidation(card,"true");
	}
	
	@Then("^(.*) warning message should be displayed$")
	public void verifyWarMsg(String warMsg) throws Throwable 
	{	
		if (warMsg.equals("Invalid number"))
		{
			UtilityFunctions.performValidation("warnMsgCardNo",warMsg);
		}
		else if (warMsg.equals("Credit card expired"))
		{
			UtilityFunctions.performValidation("warnMsgCardExp",warMsg);
		}
		
	}
	
	@Then("^It should allow to enter only 16 digits i.e (.*)$")
	public void verifyCharLimit(String cardNo) throws Throwable 
	{		
		UtilityFunctions.performValidation("txtCardNum",cardNo);	
	}
	
	@Then("^It should not accept alphabets i.e (.*)$")
	public void verifyAcceptChar(String val) throws Throwable 
	{		
		UtilityFunctions.performValidation("txtCardNum",val);
	}

	@Then("^It should list current year and additional 14 years$")
	public void verifyExpYear() throws Throwable 
	{		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		WebElement we=UtilityFunctions.retunWebElement("btnExpYear");
		Thread.sleep(1000);
		//we.click();

		WebElement we1=UtilityFunctions.retunWebElement("cmbExpYearContent");		
		Assert.assertEquals(Integer.toString(year),we1.getText());
		
		we.click();
		Thread.sleep(6000);
		WebElement we2=UtilityFunctions.retunWebElement("cmbExpYear");
		//System.out.println(we2.getAttribute("value").length());
		
	}
}
