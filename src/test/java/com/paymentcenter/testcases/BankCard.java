package com.paymentcenter.testcases;

import org.openqa.selenium.WebDriver;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utility.ExcelFunctions;
import Utility.ProjSpecificFunctions;
import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BankCard {
	
	public  WebDriver driver;
	private String data;
	public static String referenceNum;
	public static String OrderNum;
	public static String cmbMonth;
	public static String cmbYear;
	
	
	public BankCard()
	{
		driver=Hooks.driver;
	}
	

	
	@Given("^I select BankCard$")
	public void i_select_BankCard() throws Throwable {
		
		UtilityFunctions.performAction("panalBankCard","");	
		Thread.sleep(2000);

	}
	
	@Given("^I select Token$")
	public void i_select_Token() throws Throwable {
		
		UtilityFunctions.performAction("panelToken","");	
		Thread.sleep(2000);

	}
	
	@Given("^I do sale charges using body \"(.*?)\"$")
	public void i_do_sale_charges(String body) throws Throwable {
		
		ProjSpecificFunctions.doBankCardSaleTransaction(body);
		Thread.sleep(2000);

	}

	@Given("^I do credit using body \"(.*?)\"$")
	public void i_do_credit_charges(String body) throws Throwable {
		
		ProjSpecificFunctions.doBankCardCreditTransaction(body);
		Thread.sleep(2000);

	}
}


	
