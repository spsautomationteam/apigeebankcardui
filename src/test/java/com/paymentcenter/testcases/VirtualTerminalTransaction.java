package com.paymentcenter.testcases;


import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utility.ExcelFunctions;
import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class  VirtualTerminalTransaction
{
	public  WebDriver driver;
	public static String referenceNum;
	public static String OrderNum;
	public static String cmbMonth;
	public static String cmbYear;
	
	
	public VirtualTerminalTransaction()
	{
		driver=Hooks.driver;
	}
	

	@Given("^Login to Payment center application$")
		public void LoginPymntCenter() throws Exception	
		{
			Assert.assertEquals("Sage Login",driver.getTitle());			
			
			UtilityFunctions.performAction("txtEmail",ExcelFunctions.getCellData("Email",1));
			UtilityFunctions.performAction("txtPassword",ExcelFunctions.getCellData("Password",1));
			UtilityFunctions.performAction("btnSignIn","");
			Assert.assertEquals("Sage Login",driver.getTitle());
			UtilityFunctions.waitForPageToBeReady();
			UtilityFunctions.performAction("cmbMerchandID","");	
			UtilityFunctions.performAction("strMerchantID","");	
			
			UtilityFunctions.performAction("btnContinue","");		
			UtilityFunctions.waitForPageToBeReady();			
			Assert.assertEquals("Home",driver.getTitle());
			
			
		}
	@Given("^Launch Accept Payment screen$")
	public void LaunchCVT() throws Exception
	{
//		UtilityFunctions.switchBetweenTabs(0);
//		UtilityFunctions.performAction("menuHome","");
//		Hooks.driver.navigate().refresh();
//		Thread.sleep(6000);
		UtilityFunctions.navigatePaymentScreen("menuAcceptPymnt");	
		
	}	
	
	@Given("Enter data in Accept Payments > (.*) screen$")
	public void enterDataInVT(String TxMethod) throws Exception
	{
	
	    UtilityFunctions.performAction("txtOrderNum",ExcelFunctions.getCellData("strOrderNum",1));	    
		
		UtilityFunctions.performAction("txtSubTotal",ExcelFunctions.getCellData("strSubTotal",1));
		
		WebElement toClear = UtilityFunctions.retunWebElement("txtShipping");
		toClear.sendKeys(Keys.CONTROL + "a");
		toClear.sendKeys(Keys.DELETE);
		UtilityFunctions.performAction("txtShipping",ExcelFunctions.getCellData("strShipping",1));
		
		WebElement toClear1 = UtilityFunctions.retunWebElement("txtTax");
		toClear1.sendKeys(Keys.CONTROL + "a");
		toClear1.sendKeys(Keys.DELETE);
		UtilityFunctions.performAction("txtTax",ExcelFunctions.getCellData("strTax",1));
		
		if(TxMethod.equals("Credit Card"))
		{
			UtilityFunctions.strTxType = ExcelFunctions.getCellData("strCCTxType",2);
			UtilityFunctions.performAction("selectTxType",UtilityFunctions.strTxType);  
			if(UtilityFunctions.strTxType.equals("Force"))
			{
				UtilityFunctions.performAction("txtAuthCode",ExcelFunctions.getCellData("strAuthCode",1));
			}
			UtilityFunctions.performAction("txtCardNumber",ExcelFunctions.getCellData("strCardNumber",1));	
		}
		else
		{
			UtilityFunctions.strTxType = ExcelFunctions.getCellData("strACHTxType",2);
			UtilityFunctions.performAction("selectTxType",UtilityFunctions.strTxType);  
			UtilityFunctions.performAction("txtRoutingNumber",ExcelFunctions.getCellData("strRoutingNumber",1));
			UtilityFunctions.performAction("txtAccountNumber",ExcelFunctions.getCellData("strAccountNumber",1));		
			
		}			

		UtilityFunctions.performAction("txtBillingFirstName",ExcelFunctions.getCellData("strFirstName",1));
		UtilityFunctions.performAction("txtBillingLastName",ExcelFunctions.getCellData("strLastName",1));
		UtilityFunctions.performAction("txtBillingEmail",ExcelFunctions.getCellData("strEmail",1));
		
		UtilityFunctions.performAction("txtBillingAddress",ExcelFunctions.getCellData("strAddress",1));
		UtilityFunctions.performAction("txtBillingCity",ExcelFunctions.getCellData("strCity",1));
		UtilityFunctions.performAction("selectBillingState",ExcelFunctions.getCellData("strState",1));
		UtilityFunctions.performAction("txtBillingZip",ExcelFunctions.getCellData("strZip",1));
		UtilityFunctions.performAction("selectBillingCountry",ExcelFunctions.getCellData("strCountry",1));			
		UtilityFunctions.performAction("txtBillingNotes",ExcelFunctions.getCellData("strNotes",1));	
		
	}
	
	@Given("Enter different shipping data in Accept Payments screen$")
	public void enterShippingDataInVT() throws Exception
	{
		UtilityFunctions.performAction("txtShippingFirstName",ExcelFunctions.getCellData("strFirstName",2));
		UtilityFunctions.performAction("txtShippingLastName",ExcelFunctions.getCellData("strLastName",2));
				
		UtilityFunctions.performAction("txtShippingAddress",ExcelFunctions.getCellData("strAddress",2));
		UtilityFunctions.performAction("txtShippingCity",ExcelFunctions.getCellData("strCity",2));
		UtilityFunctions.performAction("selectShippingState",ExcelFunctions.getCellData("strState",2));
		UtilityFunctions.performAction("txtShippingZip",ExcelFunctions.getCellData("strZip",2));
		UtilityFunctions.performAction("selectShippingCountry",ExcelFunctions.getCellData("strCountry",2));			
	
	}
	
	@Given("Enter data in Credit Card Payment method section$")
	public void enterCCPymntData() throws Exception
	{
		UtilityFunctions.performAction("txtCardNumber",ExcelFunctions.getCellData("strCardNumber",1));	
		cmbMonth = (UtilityFunctions.retunWebElement("cmbExpirationMonth")).getText();
		System.out.println(cmbMonth); 
		UtilityFunctions.performAction("cmbExpirationMonth",cmbMonth+1);		
		cmbYear = (UtilityFunctions.retunWebElement("cmbExpirationYear")).getText();		
		UtilityFunctions.performAction("cmbExpirationYear",cmbYear+1);
		UtilityFunctions.performAction("txtCVV",ExcelFunctions.getCellData("strCVV",1));
	
	}
	
	@Given("Select Virtual Check option$")
	public void clickAch() throws Exception
	{
		Thread.sleep(2000);
		UtilityFunctions.performAction("labelVirtualCheck","");
		Thread.sleep(2000);
	}
	
	
	
		
	@When("^Perform a creditcard (.*) Transaction using card (.*)$")
		public void performCCTransaction(String TxType,String CardNo) throws Exception
		{
		//	UtilityFunctions.navigatePaymentScreen("menuAcceptPymnt");
			UtilityFunctions.performCCTransaction(TxType,CardNo);
			
		}

	
	@When("^Enter null data in required fields$")
	public void enterNoData(DataTable invalidData) throws Throwable 
	{		
		List<List<String>> data = invalidData.raw();
		WebElement toClear = UtilityFunctions.retunWebElement(data.get(0).get(0));
		toClear.sendKeys(Keys.CONTROL + "a");
		toClear.sendKeys(Keys.DELETE);
		Thread.sleep(1000);		
		UtilityFunctions.retunWebElement(data.get(0).get(0)).sendKeys(Keys.TAB);
	
	}
	
	@When("^Enter wrong data in required fields$")
	public void enterInvalidData(DataTable invalidData) throws Throwable 
	{		
		List<List<String>> data = invalidData.raw();
		WebElement toClear = UtilityFunctions.retunWebElement(data.get(0).get(0));
		toClear.sendKeys(Keys.CONTROL + "a");
		toClear.sendKeys(Keys.DELETE);
		Thread.sleep(3000);
		UtilityFunctions.performAction(data.get(0).get(0),data.get(0).get(1));
		UtilityFunctions.retunWebElement(data.get(0).get(0)).sendKeys(Keys.TAB);
	
	}

			
	@When("^Perform a creditcard Transaction for amount (.*), Tax (.*) and Shipping (.*)$")
	public static void performCCTx(String Amount,String Tax, String Shipping) throws Exception
		{
		UtilityFunctions.navigatePaymentScreen("menuAcceptPymnt");
		performCCTransaction(Amount,Tax,Shipping);
		
		}
		
	@When("^Perform a VirtualCheck Transaction for amount (.*), Tax (.*) and Shipping (.*)$")
	public static void performACHTx(String Amount,String Tax, String Shipping) throws Exception
	{
	UtilityFunctions.navigatePaymentScreen("menuAcceptPymnt");
	performACHTransaction(Amount,Tax,Shipping);
	
	}
	
	@When("^Perform a VirtualCheck (.*) Transaction$")
	public void performACHTransaction(String TxType) throws Exception
	{
		UtilityFunctions.navigatePaymentScreen("menuAcceptPymnt");
		UtilityFunctions.performACHTransaction(TxType);
		
	}
	
	@When("^HideExtraFields is checked$")
	public void hideExtraFields() throws Exception
	{		
		UtilityFunctions.performAction("labelHideExtraField","");				
	}
	
	@When("^Perform a transaction by hiding extra fields$")
	public static void TxHidingExtraFields() throws Exception
	{		
		performCCTxHideExtraFields();				
	}
	
	@When("^Cancel is clicked and No option is selected$")
	public void clickCancelNo() throws Exception
	{
		UtilityFunctions.performAction("btnCancel","");
		Thread.sleep(5000);
		Assert.assertTrue(UtilityFunctions.retunWebElement("popupCancel").isDisplayed());
		UtilityFunctions.performAction("btnCancelNo","");
		Thread.sleep(5000);
	}
	
	@When("^Cancel is clicked and Yes option is selected$")
	public void clickCancelYes() throws Exception
	{
		UtilityFunctions.performAction("btnCancel","");
		Thread.sleep(5000);
		Assert.assertTrue(UtilityFunctions.retunWebElement("popupCancel").isDisplayed());
		UtilityFunctions.performAction("btnCancelYes","");
		Thread.sleep(5000);
	}
	
	
	@When("^Add Shipping Address is clicked$")
	public void clickAddShippingAdd() throws Exception
	{
		UtilityFunctions.performAction("btnAddShippingAddress","");		
		
	}
	
	@When("^Remove Shipping Address is clicked$")
	public void clickRemoveShippingAdd() throws Exception
	{
		UtilityFunctions.performAction("btnRemoveShippingAddress","");		
		
	}
	
	@When("^Select checkbox Use Billing Address$")
	public void selectUseBillAddress() throws Exception
	{
		UtilityFunctions.performAction("labelUseBilAddress","");		
		
	}
	
	@When("^Perform the transaction with above data$")
	public void clickSubmit() throws Exception
	{
		UtilityFunctions.performAction("btnSubmit","");		
		
	}
	
	@When("^Clear button is clicked$")
	public void clickClear() throws Exception
	{
		UtilityFunctions.performAction("btnClear","");		
		
	}
	
	
	@When("^Application is idle for 10 minutes$")
	public void createAppIdleTimw() throws Exception
	{
		String strObjProp =  Hooks.proprty.getProperty("popupSessionIdle");
		String[] propParameters = strObjProp.split(",",2);
		String strValue = propParameters[1];
		new WebDriverWait(Hooks.driver,1200).until(ExpectedConditions.presenceOfElementLocated(By.xpath(strValue)));
		
		//https://www.sageexchange.com/OAuth/Login#/PaymentCenterVirtualTerminal?client_id=568022C7-4F74-425B-8A44-982F20C20816&redirect_uri=https:%2F%2Fwww.sageexchange.com%2FProcessPayment%2FIntegration%2FAuthorize&state=&gatewayId=999999999997
		
	}
	

	
	
	
	
	
	
	
		
	@Then("^(.*) response should be displayed$")
		public void verifyResponse(String responseMsg) throws Exception
		{
			UtilityFunctions.waitForPageToBeReady();			
			Assert.assertEquals(responseMsg,UtilityFunctions.retunWebElement("labelResponseMsg").getText());
			referenceNum = UtilityFunctions.retunWebElement("labelReference").getText();
			OrderNum = UtilityFunctions.retunWebElement("labelOrderNum").getText();
			
			UtilityFunctions.performAction("btnClose","");
			UtilityFunctions.waitForPageToBeReady();
			
		}
	
	
	@Then("^Warning message should be displayed$")
	public void verifyWarMsg(DataTable warMsg) throws Throwable {
		List<List<String>> data = warMsg.raw();
		Assert.assertTrue(UtilityFunctions.retunWebElement(data.get(0).get(1)).getText().contains(data.get(0).get(0)));
	
	}
	
	@Then("^It should hide Address,City,Zip,State,country fields$")
	public void verifyHidedFields() throws IOException
	{
		Assert.assertFalse(UtilityFunctions.retunWebElement("txtBillingAddress").isDisplayed());
		Assert.assertFalse(UtilityFunctions.retunWebElement("txtBillingCity").isDisplayed());
		Assert.assertFalse(UtilityFunctions.retunWebElement("selectBillingState").isDisplayed());
		Assert.assertFalse(UtilityFunctions.retunWebElement("txtBillingZip").isDisplayed());
		Assert.assertFalse(UtilityFunctions.retunWebElement("selectBillingCountry").isDisplayed());
		
	}	
	
	@Then("^Data in (.*) screen should remain$")
	public void verifyVTData(String TxMethod) throws Exception
	{
		Thread.sleep(6000);
		if(TxMethod.equals("Credit Card"))
		{
			Assert.assertEquals( ExcelFunctions.getCellData("strCCTxType",2),new Select(UtilityFunctions.retunWebElement("selectTxType")).getFirstSelectedOption().getText());
			Assert.assertEquals( ExcelFunctions.getCellData("strCardNumber",1),UtilityFunctions.retunWebElement("txtCardNumber").getAttribute("value"));
		}
		else
		{
			Assert.assertEquals( ExcelFunctions.getCellData("strACHTxType",2),new Select(UtilityFunctions.retunWebElement("selectTxType")).getFirstSelectedOption().getText());
			Assert.assertEquals( ExcelFunctions.getCellData("strRoutingNumber",1),UtilityFunctions.retunWebElement("txtRoutingNumber").getAttribute("value"));
			Assert.assertEquals( ExcelFunctions.getCellData("strAccountNumber",1),UtilityFunctions.retunWebElement("txtAccountNumber").getAttribute("value"));
			
		}
		
    	Assert.assertEquals( ExcelFunctions.getCellData("strOrderNum",1),UtilityFunctions.retunWebElement("txtOrderNum").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strSubTotal",1),UtilityFunctions.retunWebElement("txtSubTotal").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strShipping",1),UtilityFunctions.retunWebElement("txtShipping").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strTax",1),UtilityFunctions.retunWebElement("txtTax").getAttribute("value"));
		
		Assert.assertEquals( ExcelFunctions.getCellData("strFirstName",1),UtilityFunctions.retunWebElement("txtBillingFirstName").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strLastName",1),UtilityFunctions.retunWebElement("txtBillingLastName").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strEmail",1),UtilityFunctions.retunWebElement("txtBillingEmail").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strAddress",1),UtilityFunctions.retunWebElement("txtBillingAddress").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strCity",1),UtilityFunctions.retunWebElement("txtBillingCity").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strState",1),new Select(UtilityFunctions.retunWebElement("selectBillingState")).getFirstSelectedOption().getText());
		Assert.assertEquals( ExcelFunctions.getCellData("strZip",1),UtilityFunctions.retunWebElement("txtBillingZip").getAttribute("value"));
		Assert.assertEquals( ExcelFunctions.getCellData("strCountry",1),new Select(UtilityFunctions.retunWebElement("selectBillingCountry")).getFirstSelectedOption().getText());
		Assert.assertEquals( ExcelFunctions.getCellData("strNotes",1),UtilityFunctions.retunWebElement("txtBillingNotes").getAttribute("value"));

		
	}
	
	@Then("^Data should reset in all the fields$")
	public void verifyVTDataReset() throws Exception
	{
		Assert.assertEquals( ExcelFunctions.getCellData("strCCTxType",1),new Select(UtilityFunctions.retunWebElement("selectTxType")).getFirstSelectedOption().getText());
		Assert.assertEquals("",UtilityFunctions.retunWebElement("txtOrderNum").getAttribute("value"));
		Assert.assertEquals("0",UtilityFunctions.retunWebElement("txtSubTotal").getAttribute("value"));
		Assert.assertEquals("0",UtilityFunctions.retunWebElement("txtShipping").getAttribute("value"));
		Assert.assertEquals("0",UtilityFunctions.retunWebElement("txtTax").getAttribute("value"));
		Assert.assertEquals("", UtilityFunctions.retunWebElement("txtCardNumber").getAttribute("value"));
		Assert.assertEquals("", UtilityFunctions.retunWebElement("txtBillingFirstName").getAttribute("value"));
		Assert.assertEquals("", UtilityFunctions.retunWebElement("txtBillingLastName").getAttribute("value"));
		Assert.assertEquals("", UtilityFunctions.retunWebElement("txtBillingEmail").getAttribute("value"));
		Assert.assertEquals("", UtilityFunctions.retunWebElement("txtBillingAddress").getAttribute("value"));
		Assert.assertEquals("", UtilityFunctions.retunWebElement("txtBillingCity").getAttribute("value"));
		Assert.assertEquals("Alabama",new Select(UtilityFunctions.retunWebElement("selectBillingState")).getFirstSelectedOption().getText());
		Assert.assertEquals("", UtilityFunctions.retunWebElement("txtBillingZip").getAttribute("value"));
		Assert.assertEquals("UNITED STATES", new Select(UtilityFunctions.retunWebElement("selectBillingCountry")).getFirstSelectedOption().getText());
		Assert.assertEquals("",UtilityFunctions.retunWebElement("txtBillingNotes").getAttribute("value"));

		
	}
	
	
	@Then("^Shipping Information section appears$")
	public void verifyShipInfoPresence() throws IOException
	{
		Assert.assertTrue(UtilityFunctions.retunWebElement("labelShippingInfo").isDisplayed());
	}
	
	@Then("^Shipping Information section disappears$")
	public void verifyShipInfoAbsence() throws IOException
	{
		Assert.assertFalse(UtilityFunctions.retunWebElement("labelShippingInfo").isDisplayed());
	}
	
	@Then("^Billing address data should be copied to Shiping address fields$")
	public void verifyShipAddData() throws Exception
	{		
		Assert.assertEquals(UtilityFunctions.retunWebElement("txtBillingFirstName").getAttribute("value"),UtilityFunctions.retunWebElement("txtShippingFirstName").getAttribute("value"));
		Assert.assertEquals(UtilityFunctions.retunWebElement("txtBillingLastName").getAttribute("value"),UtilityFunctions.retunWebElement("txtShippingLastName").getAttribute("value"));
		Assert.assertEquals(UtilityFunctions.retunWebElement("txtBillingAddress").getAttribute("value"),UtilityFunctions.retunWebElement("txtShippingAddress").getAttribute("value"));
		Assert.assertEquals(UtilityFunctions.retunWebElement("txtBillingCity").getAttribute("value"),UtilityFunctions.retunWebElement("txtShippingCity").getAttribute("value"));
		Assert.assertEquals(new Select(UtilityFunctions.retunWebElement("selectBillingState")).getFirstSelectedOption().getText(),new Select(UtilityFunctions.retunWebElement("selectShippingState")).getFirstSelectedOption().getText());
		Assert.assertEquals(UtilityFunctions.retunWebElement("txtBillingZip").getAttribute("value"),UtilityFunctions.retunWebElement("txtShippingZip").getAttribute("value"));
		Assert.assertEquals(new Select(UtilityFunctions.retunWebElement("selectBillingCountry")).getFirstSelectedOption().getText(),new Select(UtilityFunctions.retunWebElement("selectShippingCountry")).getFirstSelectedOption().getText());
		
	}
	
	@Then("^It should reset the values in CardNumber,ExpMonth,ExpYear,CVV fields$")
	public void verifyClearResetData() throws Exception
	{		
		Assert.assertEquals("",UtilityFunctions.retunWebElement("txtCardNumber").getAttribute("value"));
		Assert.assertEquals(cmbMonth,(UtilityFunctions.retunWebElement("cmbExpirationMonth")).getText());
		Assert.assertEquals(cmbYear,(UtilityFunctions.retunWebElement("cmbExpirationYear")).getText());
		Assert.assertEquals("",UtilityFunctions.retunWebElement("txtCVV").getAttribute("value"));
		
	}
	
	
	@Then("^It should popup Inactive session dialogue$")
	public void verifySessionTimeout() throws Exception
	{		
		Assert.assertTrue(UtilityFunctions.retunWebElement("popupSessionIdle").isDisplayed());
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
		//*********************************************************************
		//Perform a credit card transaction for different amount,tax,shipping value
		//*********************************************************************
		public static void performCCTransaction(String Amount,String Tax, String Shipping) throws Exception
			{
				    UtilityFunctions.waitForElementNotVisible(50000,Hooks.driver,".//div[@class='loading-spinner-holder ng-scope']");
				    
				    UtilityFunctions.strTxType = ExcelFunctions.getCellData("strCCTxType",1);
				   
				    UtilityFunctions.performAction("txtOrderNum",ExcelFunctions.getCellData("strOrderNum",1));
				    UtilityFunctions.performAction("selectTxType",UtilityFunctions.strTxType);
					if(UtilityFunctions.strTxType.equals("Force"))
					{
						UtilityFunctions.performAction("txtAuthCode",ExcelFunctions.getCellData("strAuthCode",1));
					}
					UtilityFunctions.performAction("txtSubTotal",Amount);//ExcelFunctions.getCellData("strSubTotal",1));
					
					WebElement toClear = UtilityFunctions.retunWebElement("txtShipping");
					toClear.sendKeys(Keys.CONTROL + "a");
					toClear.sendKeys(Keys.DELETE);
					UtilityFunctions.performAction("txtShipping",Shipping);//ExcelFunctions.getCellData("strShipping",1));
					
					WebElement toClear1 = UtilityFunctions.retunWebElement("txtTax");
					toClear1.sendKeys(Keys.CONTROL + "a");
					toClear1.sendKeys(Keys.DELETE);
					UtilityFunctions.performAction("txtTax",Tax);//ExcelFunctions.getCellData("strTax",1));
							
					UtilityFunctions.performAction("txtCardNumber",ExcelFunctions.getCellData("strCardNumber",1));			
					UtilityFunctions.performAction("cmbExpirationYear","");
					UtilityFunctions.performAction("listExpYearData","");
					
					UtilityFunctions.performAction("txtBillingFirstName",ExcelFunctions.getCellData("strFirstName",1));
					UtilityFunctions.performAction("txtBillingLastName",ExcelFunctions.getCellData("strLastName",1));
					UtilityFunctions.performAction("txtBillingEmail",ExcelFunctions.getCellData("strEmail",1));
					
					UtilityFunctions.performAction("txtBillingAddress",ExcelFunctions.getCellData("strAddress",1));
					UtilityFunctions.performAction("txtBillingCity",ExcelFunctions.getCellData("strCity",1));
					UtilityFunctions.performAction("selectBillingState",ExcelFunctions.getCellData("strState",1));
					UtilityFunctions.performAction("txtBillingZip",ExcelFunctions.getCellData("strZip",1));
					UtilityFunctions.performAction("selectBillingCountry",ExcelFunctions.getCellData("strCountry",1));			
					UtilityFunctions.performAction("txtBillingNotes",ExcelFunctions.getCellData("strNotes",1));			
					UtilityFunctions.performAction("btnSubmit","");
					UtilityFunctions.waitForPageToBeReady();
					
				    
			}
		
		

		//*********************************************************************
		//Perform a credit card transaction by hiding extra fields
		//*********************************************************************
		public static void performCCTxHideExtraFields() throws Exception
			{
				    UtilityFunctions.waitForElementNotVisible(50000,Hooks.driver,".//div[@class='loading-spinner-holder ng-scope']");
				    
				    UtilityFunctions.strTxType = ExcelFunctions.getCellData("strCCTxType",1);
				   
				    UtilityFunctions.performAction("txtOrderNum",ExcelFunctions.getCellData("strOrderNum",1));
				    UtilityFunctions.performAction("selectTxType",UtilityFunctions.strTxType);
					if(UtilityFunctions.strTxType.equals("Force"))
					{
						UtilityFunctions.performAction("txtAuthCode",ExcelFunctions.getCellData("strAuthCode",1));
					}
					UtilityFunctions.performAction("txtSubTotal",ExcelFunctions.getCellData("strSubTotal",1));
					
					UtilityFunctions.performAction("txtCardNumber",ExcelFunctions.getCellData("strCardNumber",1));			
					UtilityFunctions.performAction("cmbExpirationYear","");
					UtilityFunctions.performAction("listExpYearData","");
					
					UtilityFunctions.performAction("txtBillingFirstName",ExcelFunctions.getCellData("strFirstName",1));
					UtilityFunctions.performAction("txtBillingLastName",ExcelFunctions.getCellData("strLastName",1));
					UtilityFunctions.performAction("txtBillingEmail",ExcelFunctions.getCellData("strEmail",1));
					
								
					UtilityFunctions.performAction("btnSubmit","");
					UtilityFunctions.waitForPageToBeReady();
					
				    
			}
		
		//*********************************************************************
		//Perform a virtual check transaction for different amount,tax,shipping value
		//*********************************************************************
				public static void performACHTransaction(String Amount,String Tax, String Shipping) throws Exception
					{
						UtilityFunctions.waitForPageToBeReady();
					    Thread.sleep(5000);
					    UtilityFunctions.performAction("labelVirtualCheck","");
					    
					    UtilityFunctions.strTxType = ExcelFunctions.getCellData("strACHTxType",1);
					    UtilityFunctions.performAction("txtOrderNum",ExcelFunctions.getCellData("strOrderNum",1));
					    UtilityFunctions.performAction("selectTxType",UtilityFunctions.strTxType);
					    UtilityFunctions.performAction("txtSubTotal",Amount);
					    UtilityFunctions.performAction("txtShipping",Shipping);
					    UtilityFunctions.performAction("txtTax",Tax);
						//Assert.assertEquals("");			
					    UtilityFunctions.performAction("txtRoutingNumber",ExcelFunctions.getCellData("strRoutingNumber",1));
					    UtilityFunctions.performAction("txtAccountNumber",ExcelFunctions.getCellData("strAccountNumber",1));
					    UtilityFunctions.performAction("selectAccountType",ExcelFunctions.getCellData("strAccountType",1));
					    UtilityFunctions.performAction("selectOriginatorID",ExcelFunctions.getCellData("strOriginatorID",1));				
						
					    UtilityFunctions.performAction("txtBillingFirstName",ExcelFunctions.getCellData("strFirstName",1));
					    UtilityFunctions.performAction("txtBillingLastName",ExcelFunctions.getCellData("strLastName",1));
					    UtilityFunctions.performAction("txtBillingEmail",ExcelFunctions.getCellData("strEmail",1));
						
					    UtilityFunctions.performAction("txtBillingAddress",ExcelFunctions.getCellData("strAddress",1));
					    UtilityFunctions.performAction("txtBillingCity",ExcelFunctions.getCellData("strCity",1));
					    UtilityFunctions.performAction("selectBillingState",ExcelFunctions.getCellData("strState",1));
					    UtilityFunctions.performAction("txtBillingZip",ExcelFunctions.getCellData("strZip",1));
					    UtilityFunctions.performAction("selectBillingCountry",ExcelFunctions.getCellData("strCountry",1));				
					    UtilityFunctions.performAction("txtBillingNotes",ExcelFunctions.getCellData("strNotes",1));				
					    UtilityFunctions.performAction("btnSubmit","");
						
						UtilityFunctions.waitForPageToBeReady();
							
						    
					}
	
	
	
	}


