package com.paymentcenter.testcases;

import org.openqa.selenium.WebDriver;

import Utility.UtilityFunctions;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SageDevMainPage {

	public  WebDriver driver;

	public SageDevMainPage()
	{
		driver=Hooks.driver;
	}
	
	//@When("^Click on (.*) tab present at top the Page$")
	@When("^Click on '(.*)' Tab present at top of the Page$")
	public void click_on_All_Tabs_Top_Of_Page(String tabName) throws Throwable {

		UtilityFunctions.performAction(tabName, "");
		Thread.sleep(3000);
	
	}
	
	//@When("^Click on (.*) tab present at top the Page$")
		@When("^Click on '(.*)' link present at middle of the Page")
		public void click_on_All_Links_Middle_Of_Page(String linkName) throws Throwable {

			UtilityFunctions.performAction(linkName, "");
			Thread.sleep(3000);
		
		}
	
	@Then("^Verify application should navigate to '(.*)' page properly$")
	public void Verify_Application_should_navigateTo_Corresponding_Page(String pageTitle) throws Throwable {

		UtilityFunctions.performValidation("titleAllPages", pageTitle);
//		UtilityFunctions.performAction("tabHome", "");
		driver.navigate().back();
		Thread.sleep(3000);

	}
	
}