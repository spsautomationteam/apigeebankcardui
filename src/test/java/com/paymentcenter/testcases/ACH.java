package com.paymentcenter.testcases;

import org.openqa.selenium.WebDriver;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utility.ExcelFunctions;
import Utility.ProjSpecificFunctions;
import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ACH {
	
	public  WebDriver driver;
	private String data;
	public static String referenceNum;
	public static String OrderNum;
	public static String cmbMonth;
	public static String cmbYear;
	
	
	public ACH()
	{
		driver=Hooks.driver;
	}
	
	@Given("^Developer Portal is opened$")
	public void developer_Portal_is_opened() throws Throwable {

		UtilityFunctions.waitForPageToBeReady();
		UtilityFunctions.performAction("linkApiSandBox","");	
		Thread.sleep(5000); 

	}


	@Given("^I select get_ping$")
	public void i_select_get_ping() throws Throwable {
		
		UtilityFunctions.performAction("panelAchGetPing","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_status$")
	public void i_select_get_status() throws Throwable {
		
		UtilityFunctions.performAction("panelAchGetStatus","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_charges$")
	public void i_select_get_charges() throws Throwable {
		
		UtilityFunctions.performAction("panelGetCharges","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select post_charges$")
	public void i_select_post_charges() throws Throwable {
		
		
		UtilityFunctions.performAction("panelPostCharges","");		
		Thread.sleep(2000);
	}
	
	@Given("^I set query parameter \"(.*?)\" to \"(.*?)\"$")
	public void i_set_query_parameter_to(String name, String data) throws Throwable {
	   
		//UtilityFunctions.performAction("panelQueryParameter","");	
		WebElement weQueryParameter = Hooks.driver.findElement(By.name(name));
		if(!weQueryParameter.isDisplayed()) {
			
			UtilityFunctions.performAction("panelQueryParameter","");	
			
		}
		
		Thread.sleep(2000);
		
		if(data.equalsIgnoreCase("TR_REFERENCE")){  
				Hooks.driver.findElement(By.name(name)).clear();
				Hooks.driver.findElement(By.name(name)).sendKeys(Utility.ProjSpecificFunctions.TR_REFERENCE);
				Thread.sleep(1000);
			}
		else if(data.equalsIgnoreCase("TR_ORDERNO")){  
				Hooks.driver.findElement(By.name(name)).clear();
				Hooks.driver.findElement(By.name(name)).sendKeys(Utility.ProjSpecificFunctions.TR_ORDERNO);
				Thread.sleep(1000);
			}  
		else{  
				//Hooks.driver.findElement(By.name(name)).clear();
				Hooks.driver.findElement(By.name(name)).sendKeys(data);
				Thread.sleep(1000);
				//UtilityFunctions.performAction("panelQueryParameter","");
				Thread.sleep(2000);
			}  

	}
	
	@Given("^I do ACH sale using body \"(.*?)\"$")
	public void i_do_ACH_sale_charges(String body) throws Throwable {
		
		ProjSpecificFunctions.doACHSaleTransaction(body);
		Thread.sleep(2000);

	}
	
	@Given("^I do ACH credit using body \"(.*?)\"$")
	public void i_do_ACH_credit_charges(String body) throws Throwable {
		
		ProjSpecificFunctions.doACHCreditTransaction(body);
		Thread.sleep(2000);

	}
	
	@Given("^I do ACH post_token using body \"(.*?)\"$")
	public void i_do_post_token(String body) throws Throwable {
		
		ProjSpecificFunctions.doACHPostToken(body);
		Thread.sleep(2000);

	}
	
	@Given("^I do BankCard post_token using body \"(.*?)\"$")
	public void i_do_bankcard_post_token(String body) throws Throwable {
		
		ProjSpecificFunctions.doBankCardPostToken(body);
		Thread.sleep(2000);

	}
	
	@Given("^I select get_charges_detail$")
	public void i_select_post_charges_details() throws Throwable {
		
		
		UtilityFunctions.performAction("panelGetChargesDetails","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select put_charges$")
	public void i_select_put_charges() throws Throwable {
		
		
		UtilityFunctions.performAction("panelPutCharges","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select delete_charges$")
	public void i_select_delete_charges() throws Throwable {
		
		
		UtilityFunctions.performAction("panelDeleteCharges","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select post_charge_lineitems$")
	public void i_select_post_charge_lineitems() throws Throwable {
		
		UtilityFunctions.performAction("panelPostChargesLineitems","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_charges_lineitems_detail$")
	public void i_select_get_charge_lineitems() throws Throwable {
		
		UtilityFunctions.performAction("panelGetChargesLineitems","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select delete_charge_lineitems$")
	public void i_select_delete_charge_lineitems() throws Throwable {
		
		UtilityFunctions.performAction("panelDeleteChargeLineitems","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select post_credits$")
	public void i_select_post_credits() throws Throwable {
		
		UtilityFunctions.performAction("panelPostCredits","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_credits$")
	public void i_select_get_credits() throws Throwable {
		
		UtilityFunctions.performAction("panelGetCredits","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_credits_detail$")
	public void i_select_get_credits_details() throws Throwable {
		
		UtilityFunctions.performAction("panelGetCreditsDetails","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select post_credits_reference$")
	public void i_select_post_credits_reference() throws Throwable {
		
		UtilityFunctions.performAction("panelPostCreditsReference","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select delete_credits$")
	public void i_select_delete_credits() throws Throwable {
		
		UtilityFunctions.performAction("panelDeleteCredits","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_transactions$")
	public void i_select_get_transactions() throws Throwable {
		
		UtilityFunctions.performAction("panelGetTransactions","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_transactions_detail$")
	public void i_select_get_transactions_details() throws Throwable {
		
		UtilityFunctions.performAction("panelGetTransactionsDetail","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_batches$")
	public void i_select_get_batches() throws Throwable {
		
		UtilityFunctions.performAction("panelGetBatches","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_batches_current$")
	public void i_select_get_batches_current() throws Throwable {
		
		UtilityFunctions.performAction("panelGetBatchesCurrent","");		
		Thread.sleep(2000);
	}

	@Given("^I select post_batches_current$")
	public void i_select_post_batches_current() throws Throwable {
		
		UtilityFunctions.performAction("panelPostBatchesCurrent","");		
		Thread.sleep(2000);
	}
	
	
	@Given("^I select get_batches_current_summary$")
	public void i_select_get_batches_current_summery() throws Throwable {
		
		UtilityFunctions.performAction("panelGetBatchesCurrentSummary","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_batches_totals$")
	public void i_select_get_batches_totals() throws Throwable {
		
		UtilityFunctions.performAction("panelGetBatchesTotals","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select get_batches_reference$")
	public void i_select_get_batches_ref() throws Throwable {
		
		UtilityFunctions.performAction("panelGetBatchesReference","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select post-tokens$")
	public void i_select_post_tokens() throws Throwable {
		
		UtilityFunctions.performAction("panelPostToken","");		
		Thread.sleep(2000);
	}

	@Given("^I select put-token$")
	public void i_select_put_tokens() throws Throwable {
		
		UtilityFunctions.performAction("panelPutToken","");		
		Thread.sleep(2000);
	}
	
	@Given("^I select delete-token$")
	public void i_select_put_delete() throws Throwable {
		
		UtilityFunctions.performAction("panelDeleteToken","");		
		Thread.sleep(2000);
	}

	@Given("^I click on APISandBox$")
	public void i_click_on_APISandBox() throws Throwable {
		
		UtilityFunctions.performAction("linkAPISandBox","");		
		Thread.sleep(2000);
	}
	
	@Given("^I edit resource URL with transaction reference$")
	public void i_edit_resource_URL_with_transaction_reference() throws Throwable {
	
		UtilityFunctions.performAction("panelResourceUrl","");	
		Thread.sleep(2000);	
		UtilityFunctions.performAction("spanReference",Utility.ProjSpecificFunctions.TR_REFERENCE);	

	}
	
	@Given("^I edit resource URL with token$")
	public void i_edit_resource_URL_with_token() throws Throwable {
	
		UtilityFunctions.performAction("panelResourceUrl","");	
		Thread.sleep(2000);	
		UtilityFunctions.performAction("spanReference",Utility.ProjSpecificFunctions.TR_TOKEN);	

	}
	
	@Given("^I edit resource URL with batch reference$")
	public void i_edit_resource_URL_with_batch_reference() throws Throwable {
	
		UtilityFunctions.performAction("panelResourceUrl","");	
		Thread.sleep(2000);	
		UtilityFunctions.performAction("spanReference",Utility.ProjSpecificFunctions.BATCH_REFERENCE);	

	}	
	
	
	@Given("^I set body to \"(.*?)\"$")
	public void i_set_body_to(String body) throws Throwable {
		UtilityFunctions.performAction("panelPostChargesBody",body);		
		Thread.sleep(2000);	    

	}
	
	@Given("^I do post batches using body \"(.*?)\"$")
	public void i_do_post_batches(String body) throws Throwable {
		
		ProjSpecificFunctions.doPostBatch(body);
		Thread.sleep(2000);

	}
	
	@Given("^I select ach$")
	public void i_select_ach() throws Throwable {
		
		UtilityFunctions.performAction("panelACH","");	
		Thread.sleep(2000);

	}

	@When("^I click on Send this request button$")
	public void i_click_on_Send_this_request_button() throws Throwable {
		Thread.sleep(5000);
		UtilityFunctions.performAction("btnSendThisRequest","");	
		Thread.sleep(10000);

	}

	@Then("^response code should be (\\d+)$")
	public void response_code_should_be(String responseCode) throws Throwable {
		
		UtilityFunctions.performValidation("panelResponseCode", responseCode);

	}

	@Then("^response body should contain \"(.*?)\": (\\d+)$")
	public void response_message_should_be(String responseMsg1, String responseMsg2) throws Throwable {
	    
		//UtilityFunctions.performValidation("panelResponseMessage", responseMsg1 +": "+responseMsg2);
		UtilityFunctions.performValidation("panelResponseMessage", "\""+responseMsg1+"\": "+responseMsg2);
		
		
	}	
	
	@Then("^response body should contain \"(.*?)\"$")
	public void response_message_should(String responseMsg) throws Throwable {
	    
		//UtilityFunctions.performValidation("panelResponseMessage", responseMsg);
		
	if(responseMsg.contains("TR_REFERENCE")){  
			UtilityFunctions.performValidation("panelResponseMessage", Utility.ProjSpecificFunctions.TR_REFERENCE);
		}
	else if(responseMsg.contains("TR_ORDERNO")){  
		UtilityFunctions.performValidation("panelResponseMessage", Utility.ProjSpecificFunctions.TR_ORDERNO);
		}  
	else if(responseMsg.contains("BATCH_REFERENCE")){  
		UtilityFunctions.performValidation("panelResponseMessage", Utility.ProjSpecificFunctions.BATCH_REFERENCE);
		} 
	else if(responseMsg.contains("TR_TOKEN")){  
		UtilityFunctions.performValidation("panelResponseMessage", Utility.ProjSpecificFunctions.TR_TOKEN);
		}  
	else{  
		UtilityFunctions.performValidation("panelResponseMessage", responseMsg);
		}  		
		
		
	}	

}


	
