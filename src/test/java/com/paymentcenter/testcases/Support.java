package com.paymentcenter.testcases;

import java.util.List;
import org.openqa.selenium.WebDriver;

import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Support {
	
	public  WebDriver driver;

	public Support()
	{
		driver=Hooks.driver;
	}
	
	@When("^Click on 'Support' tab in 'Sage Developer User' screen present at top left$")
	public void click_on_Forums_tab() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabSupport", "");
		Thread.sleep(3000);
	}
	
	@Then("^Verify It should display the required options under 'Support' page$")
	public void Verify_Options_Under_Support_Tab(DataTable table) throws Throwable {
		
		List<List<String>> data = table.raw();	

		UtilityFunctions.performValidation("supportHeader", data.get(1).get(0));
		UtilityFunctions.performValidation("fieldEmail", "");
		UtilityFunctions.performValidation("fieldDescription", "");
		UtilityFunctions.performValidation("buttonSubmit", data.get(2).get(0));		

	}
	
	@Then("^Verify 'Email field' should be filled by the Signed Email$")
	public void Verify_Values_Under_Support_Tab(DataTable table) throws Throwable {
		
		List<List<String>> data = table.raw();	

		UtilityFunctions.performValidation("supportHeader", "Contact Support");
		UtilityFunctions.performValidation("fieldEmail", data.get(1).get(0));
		UtilityFunctions.performValidation("fieldDescription", "");
		UtilityFunctions.performValidation("buttonSubmit", "Submit");		

	}
	
	@When("^Enter Invalid data into E-mail field$")
	public void Enter_Invalid_EmailId(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	
		
		UtilityFunctions.performValidation("fieldEmail", data.get(1).get(0));
		Thread.sleep(3000);
	}
	
	@When("^Click on Submit button$")
	public void Click_On_Submit_button() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("buttonSubmit", "");
		Thread.sleep(3000);
	}
	
	@Then("^Should display warning message like 'Please enter an email address.'$")
	public void Warning_Msg_Should_Display() throws Throwable {

		UtilityFunctions.performValidation("fieldEmail", "Please enter an email address.");
		Thread.sleep(3000);
	}

	@When("^Enter Valid data into all available fields$")
	public void Enter_ValidData_Into_All_Fields(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	

		UtilityFunctions.performAction("fieldEmail", data.get(1).get(1));
		UtilityFunctions.performAction("fieldDescription", data.get(2).get(1));
		
	}

	@Then("^Should show 'Thank you, your submission has been received.' and Go back to the form  link should be present$")
	public void Verify_ThankYou_Msg() throws Throwable {
		
		UtilityFunctions.performValidation("supportHeader", "Contact Support");
		UtilityFunctions.performValidation("navigationTabSupport", "Support");
		UtilityFunctions.performValidation("navigationTabContactSupport", "Contact Support");
		UtilityFunctions.performValidation("msgThankyou", "Thank you, your submission has been received.");
		UtilityFunctions.performValidation("linkGoBack", "Go back to the form");		

	}
	
	@When("^Click on Go Back button$")
	public void Click_On_GoBack_button() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("linkGoBack", "");
		Thread.sleep(3000);
	}
	
	@Then("^It should Navigate/Go Back to the 'Contact Support form'$")
	public void Should_GoBack_ContactSupport_Page() throws Throwable {
		
		UtilityFunctions.performValidation("supportHeader", "Contact Support");
		UtilityFunctions.performValidation("fieldEmail", "");
		UtilityFunctions.performValidation("fieldDescription", "");
		UtilityFunctions.performValidation("buttonSubmit", "Submit");		

	}
	
	@When("^Enter data into Description field Exceed the limit$")
	public void Enter_ExceedData_into_Description_Field(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	

		UtilityFunctions.performAction("fieldDescription", data.get(1).get(1));
		
	}
	
	@Then("^It should show warning message 'Content limited to 500 characters, remaining: 0'$")
	public void Should_Show_WarningMsg_For_ExceedData() throws Throwable {
		
		UtilityFunctions.performValidation("dataEceedWarMsgDescrip", "Content limited to 500 characters, remaining: 0");

	}
	
}
