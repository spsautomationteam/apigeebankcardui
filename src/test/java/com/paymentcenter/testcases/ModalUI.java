package com.paymentcenter.testcases;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

//import org.testng.annotations.Test;

import com.sun.jna.platform.FileUtils;

import Utility.UtilityFunctions;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ModalUI
{
	public  WebDriver driver;
	
	public ModalUI()
	{	
	driver = Hooks.driver; 	
	}
	
	
	@Given("^In developer Portal click on PaymentsJS >Modal UI option.$")
	public void Click_ModalUI_TryIt() throws Throwable 
	{
		UtilityFunctions.performAction("imgPayJS","");
		UtilityFunctions.performAction("linkModal","");
		UtilityFunctions.performAction("btnPayJSTryIt","");
	
	}

}
